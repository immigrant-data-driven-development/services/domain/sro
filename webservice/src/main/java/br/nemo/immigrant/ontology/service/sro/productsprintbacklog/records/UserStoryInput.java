package br.nemo.immigrant.ontology.service.sro.productsprintbacklog.records;
import java.time.LocalDateTime;
public record UserStoryInput( String importance,String effort,LocalDateTime createdDate,LocalDateTime updateDate ) {
}
