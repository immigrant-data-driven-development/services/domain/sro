package br.nemo.immigrant.ontology.service.sro.productsprintbacklog.records;
import java.time.LocalDate;
public record SprintBacklogInput( LocalDate createdtDate ) {
}
