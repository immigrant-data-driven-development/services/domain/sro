package br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProject;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
public interface ScrumProjectRepository extends PagingAndSortingRepository<ScrumProject, Long>, ListCrudRepository<ScrumProject, Long> {

    Optional<IDProjection> findByName(String name);

    Optional<IDProjection> findByApplicationsExternalId(String externalId);
    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);

}
