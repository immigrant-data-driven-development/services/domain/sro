package br.nemo.immigrant.ontology.entity.sro.stakeholder.repositories;

import br.nemo.immigrant.ontology.entity.sro.stakeholder.models.StakeholderUserStory;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
public interface StakeholderUserStoryRepository extends PagingAndSortingRepository<StakeholderUserStory, Long>, ListCrudRepository<StakeholderUserStory, Long> {

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);

}
