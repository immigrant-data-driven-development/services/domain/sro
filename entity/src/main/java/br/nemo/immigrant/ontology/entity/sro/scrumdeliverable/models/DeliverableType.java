package br.nemo.immigrant.ontology.entity.sro.scrumdeliverable.models;

public enum DeliverableType {
    NOTACCEPTEDDELIVERABLE,
    ACCEPTEDDELIVERABLE
}