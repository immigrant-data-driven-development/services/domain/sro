package br.nemo.immigrant.ontology.entity.sro.scrumprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.ProductBacklog;

import br.nemo.immigrant.ontology.entity.spo.process.models.SpecificProjectProcess;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "productbacklogdefinition")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class ProductBacklogDefinition extends SpecificProjectProcess implements Serializable {




  @OneToOne(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "productbacklogdefinition")
  @Builder.Default
  private ScrumProcess scrumprocess = null;

  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        ProductBacklogDefinition elem = (ProductBacklogDefinition) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "ProductBacklogDefinition {" +
         "id="+this.id+


      '}';
  }
}
