package br.nemo.immigrant.ontology.entity.sro.scrumprocess.models;

public enum ScrumProjectType {
    SCRUMATOMICPROJECT,
    SCRUMCOMPLEXPROJECT
}