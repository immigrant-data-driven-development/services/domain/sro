package br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories;

import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.Sprint;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import java.util.Optional;

public interface SprintRepository extends PagingAndSortingRepository<Sprint, Long>, ListCrudRepository<Sprint, Long> {

    Optional<IDProjection> findByName(String name);

    Optional<IDProjection> findByApplicationsExternalId(String externalId);

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);

}
