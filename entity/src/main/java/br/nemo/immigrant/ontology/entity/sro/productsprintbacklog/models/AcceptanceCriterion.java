package br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.rsro.requirements.models.Requirement;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "acceptancecriterion")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class AcceptanceCriterion extends Requirement implements Serializable {




  private LocalDate createdtDate;

  @ManyToOne
  @JoinColumn(name = "userstory_id")
  private UserStory userstory;


  @Builder.Default
  @Enumerated(EnumType.STRING)
  private AcceptanceCriterionType acceptencecriteriontype = AcceptanceCriterionType.FUNCTIONALACCEPTANCECRITERION;


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        AcceptanceCriterion elem = (AcceptanceCriterion) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "AcceptanceCriterion {" +
         "id="+this.id+
          ", createdtDate='"+this.createdtDate+"'"+
          ", acceptencecriteriontype='"+this.acceptencecriteriontype+"'"+
      '}';
  }
}
