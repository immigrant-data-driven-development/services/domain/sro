package br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models;

public enum AcceptanceCriterionType {
    FUNCTIONALACCEPTANCECRITERION,
    NONFUNCTIONALACCEPTANCECRITERION
}