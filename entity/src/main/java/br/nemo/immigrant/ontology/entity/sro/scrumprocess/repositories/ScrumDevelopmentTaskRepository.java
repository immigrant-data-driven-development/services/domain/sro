package br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;

public interface ScrumDevelopmentTaskRepository extends PagingAndSortingRepository<ScrumDevelopmentTask, Long>, ListCrudRepository<ScrumDevelopmentTask, Long> {

    Optional<IDProjection> findByApplicationsExternalId(String externalId);

    @Modifying
    @Query("update ScrumDevelopmentTask u set u.userstory = :userstory where u.id = :id")
    void addUserStory(@Param(value = "id") Long id, @Param(value = "userstory") UserStory userstory);

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);


}
