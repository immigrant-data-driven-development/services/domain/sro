package br.nemo.immigrant.ontology.entity.sro.scrumprocess.models;

public enum ScrumDevelopmentTaskEvent {
    CREATED,
    CHANGED,

    DELETED,
    ACTIVATED,
    REVISED,
    STATECHANGED,
    ASSIGNED,
    AUTHORIZED,
    CLOSED,
    RESOLVED,
}