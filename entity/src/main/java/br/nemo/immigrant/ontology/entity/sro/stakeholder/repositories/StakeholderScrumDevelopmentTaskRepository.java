package br.nemo.immigrant.ontology.entity.sro.stakeholder.repositories;

import br.nemo.immigrant.ontology.entity.sro.stakeholder.models.StakeholderScrumDevelopmentTask;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTaskEvent;
import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;

public interface StakeholderScrumDevelopmentTaskRepository extends PagingAndSortingRepository<StakeholderScrumDevelopmentTask, Long>, ListCrudRepository<StakeholderScrumDevelopmentTask, Long> {

    Optional<IDProjection> findByInternalId(String internalId);

    Optional<StakeholderScrumDevelopmentTask> findByActivityAndEvent(Activity activity, ScrumDevelopmentTaskEvent event);

    Boolean existsByInternalId(String internalId);

}
