package br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories;

import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ProductBacklogDefinition;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
public interface ProductBacklogDefinitionRepository extends PagingAndSortingRepository<ProductBacklogDefinition, Long>, ListCrudRepository<ProductBacklogDefinition, Long> {

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);

}
