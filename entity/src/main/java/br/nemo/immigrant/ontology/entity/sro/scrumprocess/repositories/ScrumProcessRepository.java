package br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories;

import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
public interface ScrumProcessRepository extends PagingAndSortingRepository<ScrumProcess, Long>, ListCrudRepository<ScrumProcess, Long> {

    ScrumProcess findBysoftwareprojectApplicationsExternalId(String externalId);

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);


}
