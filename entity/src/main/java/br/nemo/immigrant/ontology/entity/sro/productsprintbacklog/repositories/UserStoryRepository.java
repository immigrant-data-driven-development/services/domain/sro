package br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.repositories;

import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
public interface UserStoryRepository extends PagingAndSortingRepository<UserStory, Long>, ListCrudRepository<UserStory, Long> {

    Optional<IDProjection>  findByApplicationsExternalId(String externalId);

    @Modifying
    @Query("update UserStory u set u.userstory = :userstory where u.id = :id")
    void addUserStory(@Param(value = "id") Long id, @Param(value = "userstory") UserStory userstory);

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);


}
