package br.nemo.immigrant.ontology.entity.sro.stakeholder.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTaskEvent;
import br.nemo.immigrant.ontology.entity.spo.stakeholder.models.ProjectStakeholderArtifact;
@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "projectstakeholder_id", "activity_id", "eventDate", "event" })})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class StakeholderUserStory extends  ProjectStakeholderArtifact implements Serializable {
   

    private String state;


    @Builder.Default
    @Enumerated(EnumType.STRING)
    private ScrumDevelopmentTaskEvent event = ScrumDevelopmentTaskEvent.CREATED;

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

          StakeholderScrumDevelopmentTask elem = (StakeholderScrumDevelopmentTask) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }


}
