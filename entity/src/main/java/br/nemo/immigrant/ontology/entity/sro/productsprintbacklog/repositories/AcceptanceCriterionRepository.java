package br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.repositories;

import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.AcceptanceCriterion;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
public interface AcceptanceCriterionRepository extends PagingAndSortingRepository<AcceptanceCriterion, Long>, ListCrudRepository<AcceptanceCriterion, Long> {

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);

}
