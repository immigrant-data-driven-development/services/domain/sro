package br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.sro.scrumdeliverable.models.Deliverable;
import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.ScrumDevelopmentTask;
import br.nemo.immigrant.ontology.entity.rsro.requirements.models.RequirementArtifact;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "userstory")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class UserStory extends RequirementArtifact implements Serializable {
    @Column(columnDefinition="TEXT")
    private String importance;


  @Column(columnDefinition="TEXT")
  private String effort;

  @Column(columnDefinition="TEXT")
  private String tag;


  private LocalDateTime doneDate;

  private  LocalDateTime updateDate;


  @ManyToOne
  @JoinColumn(name = "userstory_id")
  private UserStory userstory;

   @Builder.Default
  @Enumerated(EnumType.STRING)
  private UserStoryType userstorytype = UserStoryType.EPIC;


  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        UserStory elem = (UserStory) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "UserStory {" +
         "id="+this.id+
          ", importance='"+this.importance+"'"+
          ", effort='"+this.effort+"'"+
          ", createdDate='"+this.createdDate+"'"+
          ", updateDate='"+this.updateDate+"'"+
          ", userstorytype='"+this.userstorytype+"'"+
      '}';
  }
}
