package br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.repositories;

import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.ProductBacklog;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;

public interface ProductBacklogRepository extends PagingAndSortingRepository<ProductBacklog, Long>, ListCrudRepository<ProductBacklog, Long> {
    
    Optional<IDProjection>  findByApplicationsExternalId(String externalId);

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);


}
