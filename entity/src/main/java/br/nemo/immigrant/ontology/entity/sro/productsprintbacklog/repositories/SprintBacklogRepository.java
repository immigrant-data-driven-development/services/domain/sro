package br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.repositories;

import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.SprintBacklog;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;


public interface SprintBacklogRepository extends PagingAndSortingRepository<SprintBacklog, Long>, ListCrudRepository<SprintBacklog, Long> {

    Optional<IDProjection>  findByName(String name);

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);


}
