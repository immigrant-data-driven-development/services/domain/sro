package br.nemo.immigrant.ontology.entity.sro.scrumdeliverable.repositories;

import br.nemo.immigrant.ontology.entity.sro.scrumdeliverable.models.SprintDeliverable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
public interface SprintDeliverableRepository extends PagingAndSortingRepository<SprintDeliverable, Long>, ListCrudRepository<SprintDeliverable, Long> {

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);

}
