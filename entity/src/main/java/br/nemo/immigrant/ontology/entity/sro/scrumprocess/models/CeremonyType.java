package br.nemo.immigrant.ontology.entity.sro.scrumprocess.models;

public enum CeremonyType {
    PLANNING,
    DAILY,
    REVIEW,
    RETROSPECTIVE
}