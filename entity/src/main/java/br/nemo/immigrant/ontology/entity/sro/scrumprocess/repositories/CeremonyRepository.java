package br.nemo.immigrant.ontology.entity.sro.scrumprocess.repositories;

import br.nemo.immigrant.ontology.entity.sro.scrumprocess.models.Ceremony;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
public interface CeremonyRepository extends PagingAndSortingRepository<Ceremony, Long>, ListCrudRepository<Ceremony, Long> {

    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);

}
