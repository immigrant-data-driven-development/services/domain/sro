package br.nemo.immigrant.ontology.entity.sro.scrumprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;

import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.SprintBacklog;
import br.nemo.immigrant.ontology.entity.sro.scrumdeliverable.models.Deliverable;
import br.nemo.immigrant.ontology.entity.sro.productsprintbacklog.models.UserStory;
import br.nemo.immigrant.ontology.entity.eo.teams.models.Person;
import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "scrumdevelopmenttask")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class ScrumDevelopmentTask extends Activity implements Serializable {

        @Column(columnDefinition="TEXT")
        private String activityType;


        @Column(columnDefinition="TEXT")
        private String priority;


        @Column(columnDefinition="TEXT")
        private String tags;

      @ManyToOne
      @JoinColumn(name = "userstory_id")
      private UserStory userstory;

      @Builder.Default
      private LocalDateTime createdAt = LocalDateTime.now();

      @Override
      public boolean equals(Object o) {
              if (this == o) return true;
              if (o == null || this.getClass() != o.getClass()) return false;

            ScrumDevelopmentTask elem = (ScrumDevelopmentTask) o;
            return getId().equals(elem.getId());
      }

      @Override
      public int hashCode() {
        return Objects.hash(getId());
      }

      @Override
      public String toString() {
          return "ScrumDevelopmentTask {" +
             "id="+this.id+
          '}';
      }
}
