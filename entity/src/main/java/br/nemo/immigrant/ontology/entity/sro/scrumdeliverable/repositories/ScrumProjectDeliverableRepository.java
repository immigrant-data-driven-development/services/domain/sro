package br.nemo.immigrant.ontology.entity.sro.scrumdeliverable.repositories;

import br.nemo.immigrant.ontology.entity.sro.scrumdeliverable.models.ScrumProjectDeliverable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;
import java.util.Optional;
import br.nemo.immigrant.ontology.entity.base.projections.IDProjection;
public interface ScrumProjectDeliverableRepository extends PagingAndSortingRepository<ScrumProjectDeliverable, Long>, ListCrudRepository<ScrumProjectDeliverable, Long> {
    Optional<IDProjection> findByInternalId(String internalId);

    Boolean existsByInternalId(String internalId);

}
