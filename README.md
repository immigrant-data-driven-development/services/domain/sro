# Scrum Reference Ontology (SRO) WebService

## 🚀 Goal
An Webservice that implements the conceps of Scrum Reference Ontology (SRO). SRO provides a common and comprehensive conceptualization about Scrum in the software development context and can be used to support application integration.

## 📕 Domain Documentation

SQL was modified:

1. Task and User Stories` Title and Description was modified to 65535 characters.
2. The unique constrantis from Project Stkaholder was removed. 

Domain documentation can be found [here](./docs/README.md)

## ⚙️ Requirements

1. Postgresql
2. Java 17
3. Maven

## ⚙️ Stack
1. Spring Boot 3.0
2. Spring Data Rest
3. Spring GraphQL

## 🔧 Install

1) Create a database with name sro with **CREATE DATABASE sro**.
3) Remove **Unique* of Person_id and Team_id the following tables: projectstakeholder, team member and  teammembership
2) Run the command to start the webservice and create table of database:

```bash
mvn Spring-boot:run
```
## 🔧 Usage

### Using Jar

```xml
<dependency>
  <groupId>br.nemo.immigrant.ontology.entity</groupId>
  <artifactId>sro</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
```

### Using Webservice

Execute docker-compose:
```bash
docker-compose up -d 
```
## Debezium

Go to folder named *register* and performs following command to register in debezium:

```bash
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @sro-register.json
```

To delete, uses:

```bash
curl -i -X DELETE localhost:8083/connectors/sro-connector/
```


## 🔧 Usage

* Access [http://localhost:8081](http://localhost:8081) to see Swagger
* Access [http://localhost:8081/grapiql](http://localhost::8081/grapiql) to see Graphql.

## ✒️ Team

* **[Paulo Sérgio dos Santos Júnior](paulossjunior@gmail.com)**

## 📕 Literature

* **[From a Scrum Reference Ontology to the Integration of Applications for Data-Driven Software Development](https://www.researchgate.net/publication/350158531_From_a_Scrum_Reference_Ontology_to_the_Integration_of_Applications_for_Data-Driven_Software_Development)**

* **[SEON: A Software Engineering Ontology Network](https://nemo.inf.ufes.br/wp-content/uploads/2016/10/SEON_A-Software-Engineering-Ontology-Network-Ruy-et-al.-2016.pdf)**

* **[SEON Website](https://dev.nemo.inf.ufes.br/seon/)**
