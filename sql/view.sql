-- View that return difference between creation and performing 
create view project_us_task_difference_time_view as SELECT 
product_backlog.name as project,
userstory.id as userstory_id,
userstory.name as us,
userstory.userstorytype,
userstory.created_date::date,
activity.id as activity_id,
activity.name as activity,
min(activity.created_date::date) as activity_created_date,
min(sprint_activity.event_date::date) as sprint_event, 
(sprint_activity.event_date::date)-(activity.created_date::date) as difference_created_added
from specificprojectprocess sprint 
inner join specificprojectprocess_activity sprint_activity on sprint.id  = sprint_activity.specificproject_id  and sprint.dtype = 'Sprint'
inner join activity activity  on activity.id = sprint_activity.activity_id 
inner join activity_artifact on activity_artifact.activity_id = activity.id 
inner join artifact userstory on userstory.id = activity_artifact.artifact_id 
inner join artifact_artifact on artifact_artifact.artifact_to_id = userstory.id 
inner join artifact product_backlog on product_backlog.id = artifact_artifact.artifact_from_id 
where 
product_backlog.dtype = 'ProductBacklog'
group by userstory.id, activity.id  , sprint_activity.event_date , product_backlog.name
order by product_backlog.name, userstory."name"  

create view waiting_time_view as select 
ROUND(avg(difference_created_added),2) as average_waiting_day,
project 
from sro.public.project_us_task_difference_time_view group by project